﻿
#include <iostream>

using namespace std;
int main()
{
    string str = "qwerty";
    cout << "String: " << str << "\n";
    cout << "First char is " << str.at(0) << "\n";
    cout << "Str length is " << str.length() << "\n";
    cout << "Last char is " << str.at(str.length() - 1) << "\n";
}
